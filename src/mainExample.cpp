#include "graph2occupancy.h"



using namespace std;
using namespace Eigen;
using namespace g2o;




int main(int argc, char **argv){

	string g2oFilename;

	if (argc == 1){
		g2oFilename = "robot-0-testmrslam.g2o";
	}
	else if (argc == 2){
		g2oFilename = argv[1];
	}
	else {
		exit(0);
	}


if (!ifstream(g2oFilename)){
	cout << "File not found "<<endl;
	exit(0);
}
	

	//Initialize ROS
	ros::init(argc, argv, "graph2occupancy");

	string output = "occupancygrid";
  	string topic = "grid";


	// Load graph
  	typedef BlockSolver< BlockSolverTraits<-1, -1> >  SlamBlockSolver;
  	typedef LinearSolverCSparse<SlamBlockSolver::PoseMatrixType> SlamLinearSolver;
  	SlamLinearSolver *linearSolver = new SlamLinearSolver();
  	linearSolver->setBlockOrdering(false);
  	SlamBlockSolver *blockSolver = new SlamBlockSolver(linearSolver);
  	OptimizationAlgorithmGaussNewton *solverGauss = new OptimizationAlgorithmGaussNewton(blockSolver);
  	SparseOptimizer *graph = new SparseOptimizer();
  	graph->setAlgorithm(solverGauss);    
  	graph->load(g2oFilename.c_str());

  	//graph is a pointer 
  	Graph2occupancy converter(graph, topic);

  	converter.computeMap();

  	converter.saveMap(output);


 

  	ros::Rate loop_rate(10);

 	int count = 0;

 	while (ros::ok()){

 		converter.publishMap(count);

 		loop_rate.sleep();

 	}









}