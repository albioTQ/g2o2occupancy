# README #

This package allows to convert a **g2o** graph into an occupancy grid compatible with ROS/Stage.

It has been forked by the g2o2ros node created by mtlazaro.

### Requirements ###

* OpenCV
* [g2o](https://github.com/RainerKuemmerle/g2o) (and dependencies therein)
* [ROS](http://www.ros.org/)

### Installation ###

* Clone the repository in your catkin workspace
* Compile with ``catkin_make``

### How to test it? ###

Run the mainExample node using as input a 2D graph in **g2o** format created with your favourite mapper.

In a terminal type:

``$ rosrun g2o2ros mainExample input_graph.g2o ``

It will create a output_map.png and output_map.yaml compatible with ROS/Stage and will publish the output_map.png to a  ROS topic called /grid .

Optional parameters can be visualized running the node with the ``-h`` option.

Hint: You will probably want to tune the ``-usable_range`` and ``-resolution`` options among others.

### Implementing the g2o2occupancy class ###
The graph2occupancy class has been designed to be implemented into other projects.

It takes a set of parameters and a g2o graph object as input and allows to publish the corresponding occupancy grid into a ROS topic.

